module comparator(input x,
  input y,
  output isEqual
  );

assign isEqual = (x & y) || (~x & ~y);

endmodule
