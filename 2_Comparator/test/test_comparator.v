`timescale 1ns / 1ps

module test_comparator;

  reg x, y;
  wire isEqual;
  comparator uut(
    .x(x),
    .y(y),
    .isEqual(isEqual)
    );

  initial begin
  x = 0;
  y = 0;
  #10 x = 1;
  #10 y = 1;
  #10 x = 0;
  #10 y = 0;
  #20;
  end

  initial begin
  $dumpfile("out.vcd");
  $dumpvars(0, test_comparator);
  end

  initial begin
  $monitor("x = %d, y = %d, z = %d", x, y, isEqual);
  end

endmodule
