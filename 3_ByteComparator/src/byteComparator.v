`timescale 1ns/1ps

module byteComparator (
  input [7:0] xByte, // MSB = 7, LSB = 0
  input [7:0] yByte,
  output areEqual
  );

  integer i;

  always @ (xByte or yByte) begin
    areEqual <= 1;
    for (i = 0; i < 8; i = i + 1) begin
      areEqual <= areEqual & (xByte[i] == yByte[i]);
    end
  end

endmodule
