`timescale 1ns / 1ps

module test_byteComparator;

  reg x[7:0], y[7:0];
  wire areEqual;
  byteComparator uut(
    .xByte(x),
    .yByte(y),
    .areEqual(areEqual)
    );

  initial begin
  x = 8'b00000000;
  y = 8'b00000000;
  #10 x = 8'b00000001;
  #10 y = 8'b00000001;
  #10 x = 8'b00000010;
  #10 y = 8'b00000010;
  #20;
  end

  initial begin
  $dumpfile("out.vcd");
  $dumpvars(0, test_byteComparator);
  end

  initial begin
  $display("x = %d, y = %d, z = %d", x, y, areEqual);
  end

endmodule
